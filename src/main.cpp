/*
 * Name: Demetri Van Sickle
 * Date: 4/7/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Runs the application
 */

#include <iostream>
#include "application.h"

int main()
{
    int ret = 0;

    App app;

    do
    {
        ret = app.displayMainMenu();
    } while (ret <= 0);

    return 0;
}