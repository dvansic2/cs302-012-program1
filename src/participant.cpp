/*
 * Name: Demetri Van Sickle
 * Date: 4/6/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class implementations that make up the participantManager
 */

#include <iostream>
#include "participant.h"

//********************************************************************************
//BEGIN ParticipantManager
//********************************************************************************
ParticipantManager::ParticipantManager(void)
{
}

ParticipantManager::~ParticipantManager(void)
{
    int itemsToFree = participantsList.size(); //Number of items to dealocate

    //Dealocate pointers in the list
    if (itemsToFree == 0)
    {
        return;
    }
    else
    {
        freeListR(itemsToFree);
    }
    
}

ParticipantManager::ParticipantManager(const ParticipantManager & pmToCopy)
{
    //Copy all participants recursively
    copyListR(pmToCopy, pmToCopy.participantsList.begin());
}

void ParticipantManager::freeListR(int itemsToFree)
{   
    //Base case
    if (itemsToFree == 0)
    {
        return;
    }
    else
    {   
        //Delete object
        Participant * obj = participantsList.front();
        participantsList.pop_front();
        delete obj;

        freeListR(itemsToFree - 1);
    }
}

bool ParticipantManager::add(const std::string & name, const int age)
{
    //Validate input
    if (name.empty() || age < 0)
    {
        return false;
    }

    //Create new participant object
    Participant * obj = new Participant(name, age);

    //Add to list
    participantsList.push_back(obj);

    return true;
}

bool ParticipantManager::remove(const std::string & name)
{
    //Validate input
    if (name.empty() || participantsList.empty())
    {
        return false;
    }
    
    //Get pointer to object
    Participant * obj = returnParticipant(name);

    if (obj == nullptr)
    {
        return false;
    }

    //remove from list
    participantsList.remove(obj);

    //Delete object
    delete obj;

    return true;
}

bool ParticipantManager::participantExists(const std::string & name) const
{
    //Validate input
    if (name.empty() || participantsList.empty())
    {
        return false;
    }
    
    //Get pointer to object
    Participant * obj = returnParticipant(name);

    //Return true if object exists
    return(obj != nullptr);
}

void ParticipantManager::printAllParticipants(void) const
{
    //Dont print an empty list
    if (participantsList.empty())
    {
        return;
    }

    //Iterate through list and print each object
    for (std::list<Participant*>::const_iterator it = participantsList.begin(); it != participantsList.end(); ++it)
    {
        (*it)->display();
    }
}

Participant * ParticipantManager::returnParticipant(const std::string & name) const
{   
    //Validate input
    if (name.empty() || participantsList.empty())
    {
        return nullptr;
    }
    else
    {
        return returnParticipantR(participantsList.begin(), name);
    }
}

Participant * ParticipantManager::returnParticipantR(std::list<Participant *>::const_iterator it, const std::string & nameToFind) const
{
    //Base case - end of list
    if (it == participantsList.end())
    {
        return nullptr;
    }

    //Check if name matches
    if ((*it)->getName() == nameToFind)
    {
        //Return pointer to object
        return *it;
    }
    else
    {
        return returnParticipantR(std::next(it), nameToFind);
    }
   
}

void ParticipantManager::copyListR(const ParticipantManager & pmToCopy, std::list<Participant*>::const_iterator it)
{
    //Base case - end of list
    if (it == pmToCopy.participantsList.end())
    {
        return;
    }

    //Create new participant object
    Participant * obj = new Participant((*it)->getName(), (*it)->getAge());

    //Add to list
    participantsList.push_back(obj);

    copyListR(pmToCopy, std::next(it));
}

int ParticipantManager::getTotal(void) const
{
    return participantsList.size();
}
//********************************************************************************
//END ParticipantManager
//********************************************************************************


//********************************************************************************
//BEGIN Participant 
//  NOTE: This class exists purely b/c I cant use a struct, would have otherwise
//            used a struct
//********************************************************************************
Participant::Participant(const std::string & name, const int age): name(name), age(age)
{
}

Participant::~Participant(void)
{
}

void Participant::display(void)
{
    std::cout << "    |-- Name: " << name << ", Age: " << age << std::endl;
}

int Participant::getAge(void)
{
    return age;
}

std::string Participant::getName(void)
{
    return name;
}
//********************************************************************************
//END Participant
//********************************************************************************






