/*
 * Name: Demetri Van Sickle
 * Date: 4/6/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class implementations that make up the event hierarchy
 */
#include <iostream>
#include <cstring>
#include <time.h>
#include <cstdlib>
#include "event.h"

//********************************************************************************
//BEGIN Event
//********************************************************************************
Event::Event(const char * eventName, const float price, const int startTime, const int endTime, const int minAge = DFLT_EVNT_MIN_AGE) : 
        price(price), startEndTime(2, 0), limit(DFLT_EVNT_PARTICIPANT_LMT), minAge(minAge)
{
    //Make sure event name is not null
    if (eventName == nullptr)
    {
        //Set the event name to the default name
        this->eventName = new char[strlen(DFLT_EVNT_NAME) + 1];
        strcpy(this->eventName, DFLT_EVNT_NAME);
    }
    else
    {
        //Allocate memory for new event name and copy over the event name
        this->eventName = new char[strlen(eventName) + 1];
        strcpy(this->eventName, eventName);
    }

    //Sanatize start time
    if (startTime < 0 || startTime > 24)
    {
        startEndTime[0] = DFLT_EVNT_STRT_TIME;
    }
    else
    {
        startEndTime[0] = startTime;
    }

    //Sanatize end time
    if (endTime < startTime || endTime < 0 || endTime > 24)
    {
        startEndTime[1] = startTime + DFLT_EVNT_END_TIME_OFFSET;
    }
    else
    {
        startEndTime[1] = endTime;
    }

    //Sanatize price
    if (price < 0)
    {
        this->price = DFLT_EVNT_PRICE;
    }

    //Sanatize minAge
    if (minAge < 0)
    {
        this->minAge = DFLT_EVNT_MIN_AGE;
    }
}

Event::~Event()
{
    delete[] eventName;
}

Event::Event(const Event & eventToCopy) : participants(eventToCopy.participants)
{
    //Allocate memory for new event name
    eventName = new char[strlen(eventToCopy.eventName) + 1];

    //Copy over event name
    strcpy(eventName, eventToCopy.eventName);

    //Copy over other variables
    price = eventToCopy.price;
    startEndTime = eventToCopy.startEndTime;
    limit = eventToCopy.limit;
    minAge = eventToCopy.minAge;
}

bool Event::limitReached(void) const 
{   
    //Check if limit reached
    return(participants.getTotal() >= limit);
}

bool Event::changeLimit(const int newLimit)
{   
    //Validate input
    if (newLimit <= 0)
    {
        return false;
    }

    //Change limit
    limit = newLimit;
    return true;
}

bool Event::personInEvent(const std::string & name) const
{
    //Check if person is in event
    return participants.participantExists(name);
}

bool Event::addParticipantWithChecks(const std::string & name, const int age)
{
    //Validate input
    if (name.empty() || age < 0)
    {
        return false;
    }

    //Check if limit has been reached, if the person is already in the event, and if the person is old enough
    if (limitReached() || personInEvent(name) || (age < minAge))
    {
        return false;
    }

    //Add participant
    return(participants.add(name, age));
}

bool Event::removeParticipant(const std::string & name)
{
    //Validate input
    if (name.empty())
    {
        return false;
    }

    //Remove participant
    return(participants.remove(name));
}

int Event::getTotalParticipants(void) const
{
    //Return total number of participants
    return participants.getTotal();
}

void Event::printStats(void) const
{
    //Print event name
    std::cout << "\n\nEvent Name: " << eventName << std::endl;

    //Print event price
    std::cout << " |--Price: $" << price << " per participant" << std::endl;

    //Print event start time
    std::cout << " |--Start time: " << startEndTime[0] << std::endl;

    //Print event end time
    std::cout << " |--End time: " << startEndTime[1] << std::endl;

    //Print event limit
    std::cout << " |--Limit: " << limit << " participants" << std::endl;

    //Print event min age
    std::cout << " |--Minimum age: " << minAge << std::endl;

    //Print event participants
    std::cout << " |--Number of participants: " << participants.getTotal() << std::endl;
    participants.printAllParticipants();
}

const char * Event::getEventName(void) const
{
    return eventName;
}

//********************************************************************************
//END Event
//********************************************************************************


//********************************************************************************
//BEGIN PettingZoo
//********************************************************************************
PettingZoo::PettingZoo(const char * eventName, const float price, const int startTime, const int endTime, const int minAge) : 
        Event(eventName, price, startTime, endTime, minAge), numOfGoats(DFLT_PETTING_ZOO_GOATS), numOfSheep(DFLT_PETTING_ZOO_SHEEP), numOfDucks(DFLT_PETTING_ZOO_DUCKS)
{
}

PettingZoo::~PettingZoo()
{
}

bool PettingZoo::changeAnimals(const int newGoats, const int newSheep, const int newDucks)
{
    //Validate input
    if (newGoats < 0 && newSheep < 0 && newDucks < 0)
    {
        return false;
    }

    //Change each animal count only if the new count is greater than or equal to 0
    if (newGoats >= 0)
        numOfGoats = newGoats;

    if (newSheep >= 0)
        numOfSheep = newSheep;

    if (newDucks >= 0)
        numOfDucks = newDucks;

    return true;
}

void PettingZoo::displayAnimals(void) const
{
    //Print animal counts
    std::cout << " |--Number of animals: " << std::endl;
    std::cout << "      |--goats: " << numOfGoats << std::endl;
    std::cout << "      |--sheep: " << numOfSheep << std::endl;
    std::cout << "      |--ducks: " << numOfDucks << std::endl;
}

void PettingZoo::printStats(void) const
{
    //Print event stats
    Event::printStats();

    //Print animal stats
    displayAnimals();
}
//********************************************************************************
//END PettingZoo
//********************************************************************************


//********************************************************************************
//BEGIN Maze
//********************************************************************************
Maze::Maze(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty) : 
        Event(eventName, price, startTime, endTime, minAge), difficulty(difficulty), type(type), curNumOfLostParticipants(0), totalNumOfLostParticipantsHelped(0)
{   
    //Seed random number generator
    srand(time(NULL));
}

Maze::~Maze()
{
}

bool Maze::changeDifficulty(const mazeDifficulties newDifficulty)
{
    //Change difficulty
    difficulty = newDifficulty;
    return true;
}

int Maze::checkForLostParticipants(void)
{   
    //Determine if we should gererate another lost participant
    if (rand() % 100 <= 70)
    {   
        //Increment number of lost participants but do not exceed total number of participants
        if (curNumOfLostParticipants + 1 <= getTotalParticipants())
        {
            ++curNumOfLostParticipants;
        }
    }

    std::cout << "\n" << curNumOfLostParticipants << " lost participants found!\n" << std::endl;
    
    return curNumOfLostParticipants;
}

bool Maze::helpLostParticipants(void)
{   
    //If there are no lost participants, we cant help them
    if (curNumOfLostParticipants == 0)
    {
        return false;
    }
    
    //Record the number of lost participants helped
    totalNumOfLostParticipantsHelped += curNumOfLostParticipants;

    std::cout << "\nYay! you helped " << curNumOfLostParticipants << " lost participants find the exit!\n" << std::endl;

    //Reset the number of lost participants (ie we helped them)
    curNumOfLostParticipants = 0;
    
    return true;
}

void Maze::printStats(void) const
{
    std::string strType; //String representation of maze type
    std::string strDifficulty; //String representation of maze difficulty

    //Print event stats
    Event::printStats();

    //Get the string representation of the maze type
    switch(type)
    {
        case mazeTypes::CORN:
            strType = "Hedge";
            break;
        case mazeTypes::HEDGE:
            strType = "corn";
            break;
        default:
            strType = "Unknown";
            break;
    }

    //Get the string representation of the maze difficulty
    switch(difficulty)
    {
        case mazeDifficulties::EASY:
            strDifficulty = "easy";
            break;
        case mazeDifficulties::MEDIUM:
            strDifficulty = "medium";
            break;
        case mazeDifficulties::HARD:
            strDifficulty = "hard";
            break;
        default:
            strDifficulty = "Unknown";
            break;
    }

    //Print maze stats
    std::cout << " |--Maze type: " << strType << std::endl;
    std::cout << " |--Maze difficulty: " << strDifficulty << std::endl;
    std::cout << " |--Current number of lost participants: " << curNumOfLostParticipants << std::endl;
    std::cout << " |--Total number of lost participants helped: " << totalNumOfLostParticipantsHelped << std::endl;
}
//********************************************************************************
//END Maze
//********************************************************************************


//********************************************************************************
//BEGIN HorseRide
//********************************************************************************
HorseRide::HorseRide(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName) : 
        Event(eventName, price, startTime, endTime, minAge)
{
    //Make sure horse name is not null
    if (horseName == nullptr)
    {
        //Set the horse name to the default name
        this->horseName = new char[strlen(DFLT_HORSE_NAME) + 1];
        strcpy(this->horseName, DFLT_HORSE_NAME);
    }
    else
    {
        //Allocate memory for new horse name and copy over the event name
        this->horseName = new char[strlen(horseName) + 1];
        strcpy(this->horseName, horseName);
    }
}

HorseRide::~HorseRide()
{
    delete[] horseName;
}

HorseRide::HorseRide(const HorseRide & horseRideToCopy) : Event(horseRideToCopy)
{
    //Allocate memory for new horse name
    horseName = new char[strlen(horseRideToCopy.horseName) + 1];

    //Copy over horse name
    strcpy(horseName, horseRideToCopy.horseName);
}

bool HorseRide::canRide(const int age) const
{
    //Check if person is old enough to ride
    return (age >= minAge);
}

bool HorseRide::changeHorse(const std::string & newHorseName)
{
    //Validate input
    if (newHorseName.empty())
    {
        return false;
    }

    //Delete old name, allocate memory for new horse name and copy over the name
    delete[] horseName;
    horseName = new char[newHorseName.length() + 1];
    strcpy(horseName, newHorseName.c_str());

    return true;
}

void HorseRide::printStats(void) const
{
    //Print event stats
    Event::printStats();

    //Print horse stats
    std::cout << " |--Horse name: " << horseName << std::endl;
}
//********************************************************************************
//END HorseRide
//********************************************************************************


//********************************************************************************
//BEGIN HorseRideNode
//********************************************************************************
HorseRideNode::HorseRideNode(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName) : HorseRide(eventName, price, startTime, endTime, minAge, horseName), next(nullptr), prev(nullptr) 
{
}

HorseRideNode::~HorseRideNode()
{
}

HorseRideNode * HorseRideNode::getNext(void) const
{
    return next;
}

void HorseRideNode::setNext(HorseRideNode * newNext)
{
    next = newNext;
}

void HorseRideNode::setPrev(HorseRideNode * newPrev)
{
    prev = newPrev;
}

HorseRideNode * HorseRideNode::getPrev(void) const
{
    return prev;
}
//********************************************************************************
//END HorseRideNode
//********************************************************************************


//********************************************************************************
//BEGIN MazeNode
//********************************************************************************
MazeNode::MazeNode(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty) : Maze(eventName, price, startTime, endTime, minAge, type, difficulty), next(nullptr), prev(nullptr) 
{
}

MazeNode::~MazeNode()
{
}

MazeNode * MazeNode::getNext(void) const
{
    return next;
}

void MazeNode::setNext(MazeNode * newNext)
{
    next = newNext;
}

void MazeNode::setPrev(MazeNode * newPrev)
{
    prev = newPrev;
}

MazeNode * MazeNode::getPrev(void) const
{
    return prev;
}
//********************************************************************************
//END MazeNode
//********************************************************************************
