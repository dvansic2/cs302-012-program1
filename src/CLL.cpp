/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class implementations that make up the CLL
 */
#include <string.h>
#include <iostream>
#include "CLL.h"


//********************************************************************************
//BEGIN CLL
//********************************************************************************
CLL::CLL() : head(nullptr), size(0)
{
}

CLL::~CLL()
{
    //Free all nodes
    freeListR(head);
}

CLL::CLL(const CLL & cllToCopy) : head(nullptr), size(cllToCopy.size)
{
    //Create new node
    MazeNode * newNode = new MazeNode(*cllToCopy.head);

    head = newNode;
    newNode->setNext(head);
    newNode->setPrev(head);

    //Copy all nodes recursively
    copycllR(cllToCopy, cllToCopy.head->getNext());
}

bool CLL::insert(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty)
{
    //Check if event already exists
    if (retreive(eventName) != nullptr)
    {
        return false;
    }
    
    //Create new node
    MazeNode * newNode = new MazeNode(eventName, price, startTime, endTime, minAge, type, difficulty);

    //Add to list
    if (head == nullptr)
    {
        head = newNode;

        newNode->setNext(head);
        newNode->setPrev(head);
    }
    else
    {
        //Set last nodes next to point to new node
        head->getPrev()->setNext(newNode);

        //Set new node's previous to point what was the last node
        newNode->setPrev(head->getPrev());

        //Set first nodes prev to point to new node
        head->setPrev(newNode);

        //Set the new nodes next to loop back and point to head
        newNode->setNext(head);
    }

    //Increment size
    ++size;

    return true;
}

bool CLL::remove(const std::string & eventName)
{
    //validate input
    if (eventName.empty())
    {
        return false;
    }
    
    //Find node
    MazeNode * node = retreive(eventName);

    //Node not found
    if (node == nullptr)
    {
        return false;
    }

    //Node is the last element
    if (node == head && node->getNext() == head)
    {
        //Delete the node
        delete node;

        //Reset head
        head = nullptr;

        //Decrement size
        --size;

        return true;
    }

    //Node is head
    if (node == head)
    {
        //Set the "last" node to point to the new head
        head->getPrev()->setNext(head->getNext());

        //Set the new heads prev to point to what head originaly pointed to
        head->getNext()->setPrev(head->getPrev());

        //Set the head to the next element in line
        head = head->getNext();
    }

    //Node is "last" element
    else if (node == head->getPrev())
    {   
        //Set previous nodes next to point to head
        node->getPrev()->setNext(head);

        //Set heads prev to point to the previous node
        head->setPrev(node->getPrev());
    }

    //Node is in the middle
    else if (node != head && node->getNext() != head)
    {
        //Set previous node's next to point to next node
        node->getPrev()->setNext(node->getNext());

        //Set next node's previous to point to previous node
        node->getNext()->setPrev(node->getPrev());
    }

    else
    {
        return false;
    }

    //Delete node
    delete node;

    //Decrement size
    --size;

    return true;
}

bool CLL::removeAll(void)
{
    //Dont clear an empty list
    if (head == nullptr)
    {
        return false;
    }

    //Free all nodes
    freeListR(head->getNext());

    //Reset head
    head = nullptr;

    //Reset size
    size = 0;

    return true;
}

MazeNode * CLL::retreive(const std::string & eventName)
{
    //Validate input
    if (eventName.empty())
    {
        return nullptr;
    }

    //Dont search an empty list
    if (head == nullptr)
    {
        return nullptr;
    }

     //Check the first node
    if (strcmp(eventName.c_str(), head->getEventName()) == 0)
    {
        return head;
    }

    //Search rest of the nodes
    return retreiveR(eventName, head->getNext());
}

void CLL::display(void) const
{
    //Dont print the list if there isnt a list
    if (head == nullptr)
    {
        std::cout << "No events to display" << std::endl;
        return;
    }

    std::cout << "CLL Print: " << std::endl;

    //Print the the first element
    head->printStats();
    std::cout << "\n\n---------" << std::endl;

    //Print remaining elements
    displayR(head->getNext());
}

void CLL::freeListR(MazeNode * curNode)
{
    //Base case
    if (curNode == head)
    {  
        delete curNode;

        return;
    }

    freeListR(curNode->getNext());

    //Delete object
    delete curNode;
}

MazeNode * CLL::retreiveR(const std::string & eventName, MazeNode * curNode)
{
    //Base case
    if (curNode == head)
    {
        return nullptr;
    }
    
    //Check if names match
    if (strcmp(eventName.c_str(), curNode->getEventName()) == 0)
    {
        return curNode;
    }

    return retreiveR(eventName, curNode->getNext());
}

void CLL::displayR(MazeNode * curNode) const
{
    //Base case
    if (curNode == head)
    {
        return;
    }

    //Print the current event
    curNode->printStats();
    std::cout << "\n\n---------" << std::endl;

    displayR(curNode->getNext());

    return;
}

void CLL::copycllR(const CLL & cllToCopy, MazeNode * curItem)
{
    //Base case
    if (curItem == cllToCopy.head)
    {
        return;
    }

    //Create new node
    MazeNode * newNode = new MazeNode(*curItem);

    //Set last node's next to point to new node
    head->getPrev()->setNext(newNode);

    //Set new node's previous to point what was the last node
    newNode->setPrev(head->getPrev());

    //Set first node's prev to point to new node
    head->setPrev(newNode);

    //Set the new node's next to loop back and point to head
    newNode->setNext(head);
    

    copycllR(cllToCopy, curItem->getNext());

    return;
}

int CLL::isEmpty(void) const
{
    return !(size > 0);
}
//********************************************************************************
//END CLL
//********************************************************************************