// /*
//  * Name: Demetri Van Sickle
//  * Date: 4/7/24
//  * Class: CS302
//  * Assignment: Program 1
//  * Description:
//  *      Contains tests for the participantManager class and event classes
//  *      NOT MEANT TO BE GRADED
//  */

// #include <iostream>
// #include "event.h"
// #include "ARR.h"
// #include "CLL.h"
// #include "application.h"

// int main()
// {
//     // ParticipantManager pm;

//     // pm.add("Rob", 1);
//     // pm.add("Phil", 33);
//     // pm.add("John", 25);
//     // pm.add("Jane", 30);

//     // std::cout << "total: " << pm.getTotal() << std::endl;

//     // // std::cout << "John's age: " << pm.getParticipantAge("John") << std::endl;

//     // ParticipantManager pm2 = pm;

//     // if (pm.remove("John"))
//     // {
//     //     std::cout << "John removed" << std::endl;
//     // }
//     // else
//     // {
//     //     std::cout << "John not removed" << std::endl;
//     // }

//     // std::cout << "total: " << pm.getTotal() << std::endl;

//     // if (pm.participantExists("John"))
//     // {
//     //     std::cout << "John exists" << std::endl;
//     // }
//     // else
//     // {
//     //     std::cout << "John does not exist" << std::endl;
//     // }

//     // if (pm.participantExists("Jane"))
//     // {
//     //     std::cout << "Jane exists" << std::endl;
//     // }
//     // else
//     // {
//     //     std::cout << "Jane does not exist" << std::endl;
//     // }

//     // pm.~ParticipantManager();


//     // Event e1("Event1", 10.0, 5, 2, 2);

//     // if (!e1.addParticipantWithChecks("Rob", 1))
//     // {
//     //     std::cout << "Rob not added" << std::endl;
//     // }
//     // e1.addParticipantWithChecks("Phil", 33);
//     // e1.addParticipantWithChecks("John", 25);

//     // e1.printStats();

//     // Event e2 = e1;

//     // PettingZoo pz("PettingZoo", 5.0, 10, 5, 5);

//     // pz.displayAnimals();

//     // PettingZoo pz2 = pz;

//     // Maze mz("Maze", 10.0, 5, 2, 2, CORN, HARD);

//     // mz.addParticipantWithChecks("Rob", 1);
//     // mz.addParticipantWithChecks("Phil", 33);
//     // mz.addParticipantWithChecks("John", 25);
//     // mz.addParticipantWithChecks("Jane", 30);
//     // mz.addParticipantWithChecks("Bil", 34);
//     // mz.addParticipantWithChecks("Bob", 22);
//     // mz.addParticipantWithChecks("pepe", 23);
//     // mz.addParticipantWithChecks("bill", 2);
    

//     // mz.checkForLostParticipants();
//     // mz.checkForLostParticipants();
//     // mz.checkForLostParticipants();
//     // mz.checkForLostParticipants();
//     // mz.checkForLostParticipants();

//     // mz.helpLostParticipants();

//     // mz.printStats();

//     // Maze mz2 = mz;

//     // HorseRide hr("HorseRide", 15.0, 5, 2, 2, "Billy");

//     // hr.addParticipantWithChecks("Rob", 1);
//     // hr.addParticipantWithChecks("Phil", 33);
//     // hr.addParticipantWithChecks("John", 25);
//     // hr.addParticipantWithChecks("Jane", 30);

//     // hr.printStats();

//     // HorseRide hr2 = hr;

//     // LLL lll;

//     // lll.insert("Event1", 10.0, 5, 6, 2, "Billy");
//     // lll.insert("Event2", 15.0, 5, 8, 2, "Chilly");
//     // lll.insert("Event3", 20.0, 5, 10, 2, "Gilly");
//     // lll.insert("Event4", 25.0, 5, 9, 2, "Hilly");

//     // lll.display();

//     // LLL lll2 = lll;

//     // lll.remove("Event4");

//     // lll.display();

//     // lll.removeAll();

//     // lll.display();



//     // ARR arr(2);

//     // arr.insert(0, "Event1a", 10.0, 5, 6, 2, "Billy");
//     // arr.insert(0, "Event2a", 15.0, 5, 8, 2, "Chilly");   
//     // arr.insert(1, "Event1b", 20.0, 5, 10, 2, "Tipper");
//     // arr.insert(1, "Event2b", 25.0, 5, 9, 2, "Chipper");

//     // ARR arr2 = arr;

//     // std::cout << "total[0]: " << arr.isEmpty(0) << std::endl;
//     // std::cout << "total[1]: " << arr.isEmpty(1) << std::endl;

//     // arr.remove(0, "Event2a");

//     // std::cout << "total[0]: " << arr.isEmpty(0) << std::endl;

//     // // arr.display();

//     // arr.removeAll();

//     // std::cout << "total[0]: " << arr.isEmpty(0) << std::endl;
//     // std::cout << "total[1]: " << arr.isEmpty(1) << std::endl;

//     // arr.insert(0, "Event1a", 10.0, 5, 6, 2, "Billy");

//     // std::cout << "total[0]: " << arr.isEmpty(0) << std::endl;

//     // arr.display();


//     // CLL cll;

//     // cll.insert("Event1", 10.0, 5, 6, 2, CORN, MEDIUM);
//     // cll.insert("Event2", 15.0, 5, 8, 2, HEDGE, EASY);   
//     // cll.insert("Event3", 20.0, 5, 10, 2, HEDGE, HARD);
//     // cll.insert("Event4", 25.0, 5, 9, 2, CORN, HARD);

//     // CLL cll2 = cll;

//     // std::cout << "total: " << cll.isEmpty() << std::endl;

//     // cll2.display();

//     // cll.display();

//     // cll.remove("Event4");

//     // std::cout << "total: " << cll.isEmpty() << std::endl;

//     // cll.removeAll();
    
//     // std::cout << "total: " << cll.isEmpty() << std::endl;

//     // cll.display();

//     int ret = 0;

//     App app;

//     do
//     {
//         ret = app.displayMainMenu();
//     } while (ret <= 0);
    
    
    


//     return 0;
// }