/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class implementations that make up the ARR
 */
#include <string.h>
#include <iostream>
#include "ARR.h"

//********************************************************************************
//BEGIN LLL
//********************************************************************************
LLL::LLL() : head(nullptr),tail(nullptr),size(0)
{
}

LLL::~LLL()
{
    //Free all nodes
    freeListR(head);
}

LLL::LLL(const LLL & lllToCopy) : head(nullptr), tail(nullptr), size(lllToCopy.size)
{
    //Copy all nodes recursively
    copylllR(lllToCopy.head);
}

bool LLL::insert(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName)
{
    //Check if event already exists
    if (retreive(eventName) != nullptr)
    {
        return false;
    }
    
    //Create new node
    HorseRideNode * newNode = new HorseRideNode(eventName, price, startTime, endTime, minAge, horseName);

    //Add to list
    if (head == nullptr)
    {
        head = newNode;
        tail = newNode;
    }
    else
    {
        //Set last node next to point to new node
        tail->setNext(newNode);

        //Set new node's previous to point what was the last node
        newNode->setPrev(tail);

        //Set tail to new node
        tail = newNode;
    }

    //Increment size
    ++size;

    return true;
}

bool LLL::remove(const std::string & eventName)
{
    //validate input
    if (eventName.empty())
    {
        return false;
    }
    
    //Find node
    HorseRideNode * node = retreive(eventName);

    //Node not found
    if (node == nullptr)
    {
        return false;
    }

    //Node is only element
    if (node == head && node->getNext() == nullptr)
    {
        //Delete the node
        delete node;

        //Reset head and tail
        head = nullptr;
        tail = nullptr;

        //Decrement size
        --size;

        return true;
    }

    //Node is head
    if (node == head)
    {
        head = head->getNext();
    }

    //Node is tail
    if (node == tail)
    {   
        //Set previous node's next to point to nullptr
        tail->getPrev()->setNext(nullptr);

        //Set tail to previous node
        tail = tail->getPrev();
    }

    //Node is in the middle
    if (node->getPrev() != nullptr && node->getNext() != nullptr)
    {
        //Set previous node's next to point to next node
        node->getPrev()->setNext(node->getNext());

        //Set next node's previous to point to previous node
        node->getNext()->setPrev(node->getPrev());
    }

    //Delete node
    delete node;

    //Decrement size
    --size;

    return true;
}

bool LLL::removeAll(void)
{
    //Free all nodes
    freeListR(head);

    //Reset head and tail
    head = nullptr;
    tail = nullptr;

    //Reset size
    size = 0;

    return true;
}

HorseRideNode * LLL::retreive(const std::string & eventName)
{
    //Validate input
    if (eventName.empty())
    {
        return nullptr;
    }

    //Find node
    return retreiveR(eventName, head);
}

void LLL::display(void) const
{
    //Dont print the list if there isnt a list
    if (head == nullptr)
    {
        std::cout << "No events to display" << std::endl;
        return;
    }

    std::cout << "LLL Print: " << std::endl;

    //Print recursively 
    displayR(head);
}

void LLL::freeListR(HorseRideNode * curNode)
{
    //Base case
    if (curNode == nullptr)
    {
        return;
    }

    freeListR(curNode->getNext());

    //Delete object
    delete curNode;
}

HorseRideNode * LLL::retreiveR(const std::string & eventName, HorseRideNode * curNode)
{
    //Base case
    if (curNode == nullptr)
    {
        return nullptr;
    }
    
    //Check if names match
    if (strcmp(eventName.c_str(), curNode->getEventName()) == 0)
    {
        return curNode;
    }

    return retreiveR(eventName, curNode->getNext());
}

void LLL::displayR(HorseRideNode * curNode) const
{
    //Base case
    if (curNode == nullptr)
    {
        return;
    }

    //Print the current event
    curNode->printStats();
    std::cout << "\n\n---------" << std::endl;

    displayR(curNode->getNext());

    return;
}

void LLL::copylllR(HorseRideNode * curItem)
{
    //Base case
    if (curItem == nullptr)
    {
        return;
    }

    //Create new node
    HorseRideNode * newNode = new HorseRideNode(*curItem);

    //Add to list
    if (head == nullptr)
    {
        head = newNode;
        tail = newNode;
    }
    else
    {
        //Set last node next to point to new node
        tail->setNext(newNode);

        //Set new node's previous to point what was the last node
        newNode->setPrev(tail);

        //Set tail to new node
        tail = newNode;
    }

    copylllR(curItem->getNext());

    return;
}

int LLL::getSize(void) const
{
    return size;
}
//********************************************************************************
//END LLL
//********************************************************************************


//********************************************************************************
//BEGIN ARR
//********************************************************************************
ARR::ARR(const int size) : size(size)
{
    //Sanitize input
    if (size < 1)
    {
        this->size = ARR_DEFAULT_SIZE;
    }

    //Create array of LLLs
    lllArray = new LLL * [this->size] {nullptr};
}

ARR::~ARR()
{
    //Free all LLLs
    freeArrR(0);

    //Free array
    delete [] lllArray;
}

ARR::ARR(const ARR & arrToCopy) : size(arrToCopy.size)
{
    //Create array of LLLs
    lllArray = new LLL * [size] {nullptr};

    //Copy all LLLs
    copyArrR(arrToCopy, 0);
}

bool ARR::insert(const int index, const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName)
{
    //Validate input
    if (index < 0 || index >= size)
    {
        return false;
    }

    //Create LLL if it doesnt exist
    if (lllArray[index] == nullptr)
    {
        lllArray[index] = new LLL();
    }

    //Create an LLL entry
    return lllArray[index]->insert(eventName, price, startTime, endTime, minAge, horseName);
}

bool ARR::remove(const int index, const std::string & eventName)
{
    //Validate input
    if (index < 0 || index >= size)
    {
        return false;
    }

    //Remove event
    return lllArray[index]->remove(eventName);
}

bool ARR::removeAll(void)
{
    //Free all LLLs
    freeArrR(0);

    return true;
}

HorseRideNode * ARR::retreive(const int index, const std::string & eventName) const
{
    //Validate input
    if (index < 0 || index >= size)
    {
        return nullptr;
    }

    //Find event
    return lllArray[index]->retreive(eventName);
}

void ARR::display(void) const
{
    printArrR(0);
}

void ARR::copyArrR(const ARR & arrToCopy, int curIndex)
{
    //Base case
    if (curIndex == size)
    {
        return;
    }

    //Copy LLL if it exists
    if (arrToCopy.lllArray[curIndex] != nullptr)
    {
        lllArray[curIndex] = new LLL(*arrToCopy.lllArray[curIndex]);
    }

    copyArrR(arrToCopy, curIndex + 1);
}

void ARR::freeArrR(int curIndex)
{
    //Base case
    if (curIndex == size)
    {
        return;
    }

    //Free LLL if it exists
    if (lllArray[curIndex] != nullptr)
    {
        delete lllArray[curIndex];

        //Reset index
        lllArray[curIndex] = nullptr;
    }

    freeArrR(curIndex + 1);
}

void ARR::printArrR(int curIndex) const
{
    //Base case
    if (curIndex == size)
    {
        return;
    }

    //Print LLL if it exists
    if (lllArray[curIndex] != nullptr)
    {
        std::cout << "Index: " << curIndex << std::endl;
        lllArray[curIndex]->display();
    }
    else
    {
        std::cout << "Index: " << curIndex << " is empty" << std::endl;
    }


    printArrR(curIndex + 1);
}

int ARR::isEmpty(int index) const
{
    //Validate input
    if (index < 0 || index >= size)
    {
        return -1;
    }

    //Check LLL size
    if (lllArray[index]->getSize() > 0)
    {
        return 0;
    }
    else
    {
        return true;
    }
}
//********************************************************************************
//END ARR
//********************************************************************************