/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class implementations that make up the application class
 */
#include <iostream>
#include <climits>
#include "application.h"
#include "event.h"

#define HORSE_RIDE_ARR_INDEX  0


App::App(void)
{
}

App::~App(void)
{
    //free all data structures
    for(std::list<PettingZoo *>::const_iterator it = pettingZooEvents.begin(); it != pettingZooEvents.end(); ++it)
    {
        delete *it;
    }
}

int App::displayMainMenu()
{
    int optionChoiceNum = -1;  //Number of the option chosen
    int eventChoiceNum = -1;  //Number of the event chosen
    std::string usrInput;   //General input string

    //Clear screen
    clearScreen();

    //Display main menu (ORDER MUST MATCH ENUM)
    std::cout << "Welcome to the event manager!\n" << std::endl;
    std::cout << "Please select an option:" << std::endl;
    std::cout << "1. Add an event" << std::endl;
    std::cout << "2. Close an event" << std::endl;
    std::cout << "3. Add a participant" << std::endl;
    std::cout << "4. Remove a participant" << std::endl;
    std::cout << "5. List Planned Events" << std::endl;
    std::cout << "6. Event Specific Options" << std::endl;
    std::cout << "7. Exit" << std::endl;

    //Get user input
    std::cin >> usrInput;

    //Validate input
    optionChoiceNum = validateNumOption(usrInput);
    if (optionChoiceNum == INT_MIN)
    {
        std::cout << "Invalid choice. Please enter a single number" << std::endl;
        waitForEnter();
        return -1;
    }

    if (optionChoiceNum == ADD_EVENT || optionChoiceNum == CLOSE_EVENT || optionChoiceNum == ADD_PARTICIPANT || optionChoiceNum == REMOVE_PARTICIPANT 
        || optionChoiceNum == LIST_EVENTS)
    {
        //Clear screen
        clearScreen();

        //Display event type menu
        std::cout << "\nPlease select an event type:" << std::endl;
        std::cout << "1. Petting Zoo" << std::endl;
        std::cout << "2. Maze" << std::endl;
        std::cout << "3. Horse Ride" << std::endl;

        //Get user input
        std::cin >> usrInput;

        //Validate input
        eventChoiceNum = validateNumOption(usrInput);
        if (eventChoiceNum == INT_MIN || eventChoiceNum < 1 || eventChoiceNum >= EVENT_COUNT)
        {
            std::cout << "Invalid choice. Please enter a single number" << std::endl;
            waitForEnter();
            return -1;
        }

        //Execute option
        switch (optionChoiceNum)
        {
        case ADD_EVENT:
            if (!addEvent(static_cast<eventTypes>(eventChoiceNum)))
            {
                waitForEnter();
                return -1;
            }   
            break;
        case CLOSE_EVENT:
            if (!closeEvent(static_cast<eventTypes>(eventChoiceNum)))
            {
                waitForEnter();
                return -1;
            }
            break;
        case ADD_PARTICIPANT:
            if (!addParticipant(static_cast<eventTypes>(eventChoiceNum)))
            {
                waitForEnter();
                return -1;
            }
            break;
        case REMOVE_PARTICIPANT: 
            if (!removeParticipant(static_cast<eventTypes>(eventChoiceNum)))
            {
                waitForEnter();
                return -1;
            }
            break; 
        case LIST_EVENTS:
            listEvents(static_cast<eventTypes>(eventChoiceNum));
            break; 
        default:
            break;
        }
    }
    else if(optionChoiceNum == EVENT_SPECIFIC)
    {
        if(!eventSpecificMenu())
        {
            return -1;
        }
    }
    else if (optionChoiceNum == EXIT)
    {
        std::cout << "Goodbye!" << std::endl;
        return 1;
    }
    else
    {
        std::cout << "Invalid choice" << std::endl;
        return -1;
    }

    return 0;
}

bool App::eventSpecificMenu(void)
{
    std::string eventName;  //Name of the event
    bool validInput = false;    //Flag to check if input is valid
    int choiceNum = 0;          //Number of the choice
    std::string usrInput;   //General input string
    int numOfGoats; //Number of goats in the petting zoo
    int numOfSheep; //Number of sheep in the petting zoo
    int numOfDucks; //Number of ducks in the petting zoo
    int lostParticipantCnt; //Number of lost participants in the maze
    PettingZoo * pettingZooEvent = nullptr; //Pointer to petting zoo event
    Maze * mazeEvent = nullptr; //Pointer to maze event
    HorseRide * horseRideEvent = nullptr; //Pointer to horse ride event

    //Clear screen
    clearScreen();

    //Display menu
    std::cout << "\nPlease select an option:" << std::endl;
    std::cout << "1. Change petting zoo animals" << std::endl;
    std::cout << "2. Look for lost maze participants" << std::endl;
    std::cout << "3. Save lost maze participants" << std::endl;

    //Get user input
    std::cin >> usrInput;

    //Validate input
    choiceNum = validateNumOption(usrInput);
    if (choiceNum == INT_MIN || choiceNum < 1 || choiceNum >= EVENT_SPECIFIC_COUNT)
    {
        std::cout << "Invalid choice. Please enter a single number" << std::endl;
        waitForEnter();
        return false;
    }

    clearScreen();

    //Execute option
    switch (choiceNum)
    {
    case PETTING_ZOO_ANIMAL_CNG:
        //Make sure there are petting zoo events
        if(pettingZooEvents.empty())
        {
            std::cout << "\nNo Petting Zoo events to change animals for\n" << std::endl;
            waitForEnter();
            return false;
        }

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> eventName;

        //Find event
        pettingZooEvent = returnPettingZoo(eventName);
        if(pettingZooEvent == nullptr)
        {
            std::cout << "\nNo Petting Zoo event named: \""<< eventName << "\" found\n" << std::endl;
            waitForEnter();
            return false;
        }

        //Get new animal counts
        while (!validInput)
        {
            clearScreen();

            //Get new number of goats
            std::cout << "Enter new number of goats ( a value < 0 means no change): ";
            std::cin >> numOfGoats;

            //Get new number of sheep
            std::cout << "Enter new number of sheep ( a value < 0 means no change): ";
            std::cin >> numOfSheep;

            //Get new number of ducks
            std::cout << "Enter new number of ducks ( a value < 0 means no change): ";
            std::cin >> numOfDucks;

            //Validate input and handle input error
            if(std::cin.fail())
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
            else
            {
                validInput = true;
            }
        } 

        //Change animals
        if(!pettingZooEvent->changeAnimals(numOfGoats, numOfSheep, numOfDucks))
        {
            std::cout << "\nNo animal numbers were changed\n" << std::endl;
            waitForEnter();
            return false;
        }

        std::cout << "\nAnimal numbers changed successfully!" << std::endl;
        break;

    case MAZE_FIND_LOST:
        //Make sure there are maze events
        if (mazeEvents.isEmpty())
        {
            std::cout << "\nNo Maze events to find lost participants for\n" << std::endl;
            waitForEnter();
            return false;
        }

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> eventName;

        //Find event
        mazeEvent = mazeEvents.retreive(eventName);
        if(mazeEvent == nullptr)
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            waitForEnter();
            return false;
        }

        clearScreen();

        //Find lost participants
        lostParticipantCnt = mazeEvent->checkForLostParticipants();
        if(lostParticipantCnt > 0)
        {
            waitForEnter();

            //Ask user if they would like to save lost participants
            validInput = false;
            while (!validInput)
            {
                clearScreen();

                //Ask user if they want to save the lost participants
                std::cout << "Would you like to save the lost participants? (y=1/n=0): ";

                //Get user input
                std::cin >> usrInput;

                //Validate input
                choiceNum = validateNumOption(usrInput);
                if (choiceNum == INT_MIN || choiceNum < 0 || choiceNum > 1)
                {
                    std::cout << "Invalid choice. Please enter a single number" << std::endl;
                    waitForEnter();
                }
                else
                {
                    validInput = true;
                }
            } 

            if(choiceNum == 1)
            {
                if(!mazeEvent->helpLostParticipants())
                {
                    return false;
                }
            }
        }
        break;
    
    case HORSE_RIDE_NAME_HORSE:
        //Make sure there are horse ride events 
        if (horseRideEvents.isEmpty(HORSE_RIDE_ARR_INDEX))
        {
            std::cout << "\nNo Horse Ride events to find lost participants for\n" << std::endl;
            waitForEnter();
            return false;
        }

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> eventName;

        //Find event
        horseRideEvent = horseRideEvents.retreive(HORSE_RIDE_ARR_INDEX, eventName);
        if(horseRideEvent == nullptr)
        {
            std::cout << "\nNo Horse Ride event named: \""<< eventName << "\" found\n" << std::endl;
            waitForEnter();
            return false;
        }

        //Get the new horse name
        validInput = false;
        while (!validInput)
        {
            clearScreen();

            //Ask for the new name
            std::cout << "Enter the new name of the horse: ";

            //Get user input
            std::cin >> usrInput;

            //Validate input and handle input error
            if(std::cin.fail())
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
            else
            {
                validInput = true;
            }
        } 

        clearScreen();

        if(!horseRideEvent->changeHorse(usrInput))
        {
            std::cout << "\nName not changed" << std::endl;
            return false;
        }

        std::cout << "Name changed" << std::endl;
        break;

    default:
        std::cout << "Invalid eventSpecificOption given" << std::endl;
        waitForEnter();
        return false;
    };

    waitForEnter();
    return true;
}

int App::validateNumOption(const std::string & userInput, int maxDigits) const
{
    //Make sure number of digits is valid
    if (static_cast<int>(userInput.length()) > maxDigits)
    {
        return INT_MIN;
    }

    //Make sure input is a number
    for(int i = 0; i < static_cast<int>(userInput.length()); ++i)
    {
        if (std::isdigit(userInput[i]) == false)
        {
            return INT_MIN;
        }
    }
    
    return std::stoi(userInput);
}

void App::clearScreen() const
{
    //Clear screen
    std::cout << "\033[2J\033[1;1H";
}

void App::waitForEnter() const
{
    //Wait for user to press enter
    std::cout << "Press enter to continue..." << std::endl;
    std::cin.ignore(INT_MAX, '\n'); //This is ignoring all to the newline character, helps in cin.fails later in the program
    std::cin.get();
}

bool App::addEvent(const eventTypes eventType)
{
    std::string evnetName;      //Name of the event
    float price = 0.0;          //Price of the event
    int startTime = 0;          //Start time of the event
    int endTime = 0;            //End time of the event
    int minAge = 0;             //Minimum age for the event
    int mazeType;               //Type of maze
    int mazeDifficulty;         //Difficulty of maze
    bool validInput = false;    //Flag to check if input is valid
    std::string horseName;      //Name of the horse

    while (!validInput)
    {
        clearScreen();

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> evnetName;

        //Get price
        std::cout << "Enter the price of the event: ";
        std::cin >> price;

        //Get start time
        std::cout << "Enter the start time of the event: ";
        std::cin >> startTime;

        //Get end time
        std::cout << "Enter the end time of the event: ";
        std::cin >> endTime;

        //Get minimum age
        std::cout << "Enter the minimum age for the event: ";
        std::cin >> minAge;
        
        //Validate input and handle input error
        if(std::cin.fail())
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
        else
        {
            validInput = true;
        }
    }

    if(eventType == PETTING_ZOO)
    {
        //Create new petting zoo object
        PettingZoo * newPettingZoo = new PettingZoo(evnetName.c_str(), price, startTime, endTime, minAge);

        //Add to list
        pettingZooEvents.push_back(newPettingZoo);
    }
    else if(eventType == MAZE)
    {
        //Get additional information
        validInput = false;
        while (!validInput)
        {
            clearScreen();

            //Get maze type
            std::cout << "Enter the type of maze (0 = Corn, 1 = Hedge): ";
            std::cin >> mazeType;

            //Get maze difficulty
            std::cout << "Enter the difficulty of the maze (0 = Easy, 1 = Medium, 2 = Hard): ";
            std::cin >> mazeDifficulty;

            //Validate input and handle input error
            if(std::cin.fail())
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
            else if(mazeType < 0 || mazeType >= TYPE_COUNT || mazeDifficulty < 0 || mazeDifficulty >= DIFF_COUNT)
            {
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
            else
            {
                validInput = true;
            }
        }

        //Create new maze event and add it to the structure
        mazeEvents.insert(evnetName.c_str(), price, startTime, endTime, minAge, static_cast<mazeTypes>(mazeType), static_cast<mazeDifficulties>(mazeDifficulty));
    }
    else if(eventType == HORSE_RIDE)
    {
        //Get additional information
        validInput = false;
        while (!validInput)
        {
            clearScreen();

            //Get horse name
            std::cout << "Enter the name of the horse: ";
            std::cin >> horseName;

            //Validate input and handle input error    
            if(std::cin.fail())
            {
                std::cin.clear();
                std::cout << "\n\nInvalid input" << std::endl;
                waitForEnter();
            }
            else
            {
                validInput = true;
            }
        }

        //Create new horse ride event and add it to the structure
        horseRideEvents.insert(HORSE_RIDE_ARR_INDEX, evnetName.c_str(), price, startTime, endTime, minAge, horseName.c_str());
    }
    else
    {
        std::cout << "\nInvalid event type\n" << std::endl;
        return false;
    }

    std::cout << "\nEvent added successfully!" << std::endl;
    waitForEnter();

    return true;
}

bool App::closeEvent(const eventTypes eventType)
{
    std::string eventName;      //Name of the event
    bool validInput = false;    //Flag to check if input is valid

    //Get additional information
    validInput = false;
    while (!validInput)
    {
        clearScreen();

        //Get event name to close
        std::cout << "Enter the name of the event you wish to close: ";
        std::cin >> eventName;

        //Validate input and handle input error
        if(std::cin.fail())
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
        else
        {
            validInput = true;
        }
    }

    //Remove the specified event from the proper list
    if(eventType == PETTING_ZOO)
    {
        //Make sure there are events to remove
        if(pettingZooEvents.empty())
        {
            std::cout << "\nNo Petting Zoo events to close\n" << std::endl;
            return false;
        }

        //Find event
        PettingZoo * event = returnPettingZoo(eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Petting Zoo event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Remove event
        pettingZooEvents.remove(event);
    }
    else if(eventType == MAZE)
    {   
        //Make sure there are events to remove
        if (mazeEvents.isEmpty())
        {
            std::cout << "\nNo Maze events to close\n" << std::endl;
            return false;
        }

        //Remove event
        if(!mazeEvents.remove(eventName))
        {
            std::cout << "\nNo Horse Ride event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }
    }
    else if(eventType == HORSE_RIDE)
    {
        //Make sure there are events to remove
        if (horseRideEvents.isEmpty(HORSE_RIDE_ARR_INDEX))
        {
            std::cout << "\nNo Horse Ride events to close\n" << std::endl;
            return false;
        }

        //Remove event
        if(!horseRideEvents.remove(HORSE_RIDE_ARR_INDEX, eventName))
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }
    }
    else
    {
        std::cout << "\nInvalid event type\n" << std::endl;
        return false;
    }
    
    std::cout << "Event closed successfully!" << std::endl;
    waitForEnter();

    return true;
}

bool App::addParticipant(const eventTypes eventType)
{
    bool validInput = false;    //Flag to check if input is valid
    std::string eventName;      //Name of the event
    std::string participantName;    //Name of the participant
    int participantAge = 0;     //Age of the participant

    //Get additional information
    while (!validInput)
    {
        clearScreen();

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> eventName;

        //Get participant name
        std::cout << "Enter the name of the participant: ";
        std::cin >> participantName;

        //Get participant age
        std::cout << "Enter the age of the participant: ";
        std::cin >> participantAge;
        
        //Validate input and handle input error
        if(std::cin.fail())
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
        else
        {
            validInput = true;
        }
    } 

    //Add participant to the proper event
    if(eventType == PETTING_ZOO)
    {
        //Make sure there are events to remove
        if(pettingZooEvents.empty())
        {
            std::cout << "\nNo Petting Zoo events to add participants to\n" << std::endl;
            return false;
        }

        //Find event
        PettingZoo * event = returnPettingZoo(eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Petting Zoo event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->addParticipantWithChecks(participantName, participantAge))
        {
            std::cout << "\nParticipant not added\n" << std::endl;
            return false;
        }
    }
    else if(eventType == MAZE)
    {
        //Make sure there are events to remove
        if (mazeEvents.isEmpty())
        {
            std::cout << "\nNo Maze events to add participants to\n" << std::endl;
            return false;
        }

        //Fetch event
        Maze * event = mazeEvents.retreive(eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->addParticipantWithChecks(participantName, participantAge))
        {
            std::cout << "\nParticipant not added\n" << std::endl;
            return false;
        }
    }
    else if(eventType == HORSE_RIDE)
    {
        //Make sure there are events to remove
        if (horseRideEvents.isEmpty(HORSE_RIDE_ARR_INDEX))
        {
            std::cout << "\nNo Maze events to add participants to\n" << std::endl;
            return false;
        }

        //Fetch event
        HorseRide * event = horseRideEvents.retreive(HORSE_RIDE_ARR_INDEX, eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->addParticipantWithChecks(participantName, participantAge))
        {
            std::cout << "\nParticipant not added\n" << std::endl;
            return false;
        }
    }
    else
    {
        std::cout << "\nInvalid event type\n" << std::endl;
        return false;
    }

    std::cout << "Participant added successfully!" << std::endl;
    waitForEnter();

    return true;
}

bool App::removeParticipant(const eventTypes eventType)
{
    bool validInput = false;    //Flag to check if input is valid
    std::string eventName;      //Name of the event
    std::string participantName;    //Name of the participant

    //Get additional information
    while (!validInput)
    {
        clearScreen();

        //Get event name
        std::cout << "Enter the name of the event: ";
        std::cin >> eventName;

        //Get participant name
        std::cout << "Enter the name of the participant: ";
        std::cin >> participantName;
        
        //Validate input and handle input error
        if(std::cin.fail())
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
        else
        {
            validInput = true;
        }
    } 

    //Add participant to the proper event
    if(eventType == PETTING_ZOO)
    {
        //Make sure there are events to remove
        if(pettingZooEvents.empty())
        {
            std::cout << "\nNo Petting Zoo events to remove participants from\n" << std::endl;
            return false;
        }

        //Find event
        PettingZoo * event = returnPettingZoo(eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Petting Zoo event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->removeParticipant(participantName))
        {
            std::cout << "\nParticipant not removed\n" << std::endl;
            return false;
        }
    }
    else if(eventType == MAZE)
    {
        //Make sure there are events to remove
        if (mazeEvents.isEmpty())
        {
            std::cout << "\nNo Maze events to remove participants from\n" << std::endl;
            return false;
        }

        //Fetch event
        Maze * event = mazeEvents.retreive(eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->removeParticipant(participantName))
        {
            std::cout << "\nParticipant not removed\n" << std::endl;
            return false;
        }
    }
    else if(eventType == HORSE_RIDE)
    {
        //Make sure there are events to remove
        if (horseRideEvents.isEmpty(HORSE_RIDE_ARR_INDEX))
        {
            std::cout << "\nNo Maze events to remove participants from\n" << std::endl;
            return false;
        }

        //Fetch event
        HorseRide * event = horseRideEvents.retreive(HORSE_RIDE_ARR_INDEX, eventName);
        if(event == nullptr)
        {
            std::cout << "\nNo Maze event named: \""<< eventName << "\" found\n" << std::endl;
            return false;
        }

        //Add participant
        if (!event->removeParticipant(participantName))
        {
            std::cout << "\nParticipant not removed\n" << std::endl;
            return false;
        }
    }
    else
    {
        std::cout << "\nInvalid event type\n" << std::endl;
        return false;
    }

    std::cout << "Participant removed successfully!" << std::endl;
    waitForEnter();

    return true;
}

void App::listEvents(const eventTypes eventType) const
{
    if(eventType == PETTING_ZOO)
    {
        //Display all petting zoo events and their information
        for(std::list<PettingZoo *>::const_iterator it = pettingZooEvents.begin(); it != pettingZooEvents.end(); ++it)
        {
            (*it)->printStats();
        }
    }
    else if(eventType == MAZE)
    {
        mazeEvents.display();
    }
    else if(eventType == HORSE_RIDE)
    {
        horseRideEvents.display();
    }

    //Allow user to see the results before returning to the main menu
    waitForEnter();
}

PettingZoo * App::returnPettingZoo(const std::string & eventName) const
{
    //Validate input
    if (eventName.empty())
    {
        return nullptr;
    }

    //Find event
    for(std::list<PettingZoo *>::const_iterator it = pettingZooEvents.begin(); it != pettingZooEvents.end(); ++it)
    {
        if ((*it)->getEventName() == eventName)
        {
            //Return pointer to object
            return *it;
        }
    }

    return nullptr;
}