#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class declarations that make up the CLL
 */
#include "event.h"

//********************************************************************************
//BEGIN CLL
//********************************************************************************
class CLL
{
public:
    CLL();
    ~CLL();
    CLL(const CLL & cllToCopy);

    /*
     * Description:
     *      Adds a new MazeNode to the end the CLL
     * 
     * Arguments:
     *      (IN) const char * -- name of the event
     *      (IN) const float -- price of the event
     *      (IN) const int -- start time of the event
     *      (IN) const int -- end time of the event
     *      (IN) const int -- minimum age of the event
     *      (IN) mazeTypes --  enum of the type of maze (corn or hedge)    
     *      (IN) mazeDifficulties -- enum of the difficulty of the maze (easy, medium, or hard)
     * 
     * Returns:
     *      true -- event added
     *      false -- event not added
     */
    bool insert(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty);

    /*
     * Description:
     *      Removes a MazeNode from the CLL given the event name
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to remove
     * 
     * Returns:
     *      true -- event removed
     *      false -- event not removed
    */
    bool remove(const std::string & eventName);

    /*
     * Description:
     *      Removes all MazeNodes from the CLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true -- all events removed
     *      false -- no events removed
    */
    bool removeAll(void);

    /*
     * Description:
     *      Retreives a MazeNode from the CLL given the event name
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to retreive
     * 
     * Returns:
     *      MazeNode * -- pointer to the MazeNode (nullptr if not found)
    */
    MazeNode * retreive(const std::string & eventName);

    /*
     * Description:
     *      Displays all MazeNodes in the CLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      void
    */
    void display(void) const;

    /*
     * Description:
     *      Returns the number of events in the CLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int -- number of events
    */
    int isEmpty(void) const;

   
private:
    /*
     * Description:
     *      Frees all nodes in the CLL recursively. Starts with second element and finishes with the "first"
     * 
     * Arguments:
     *      (IN) MazeNode * -- pointer to the current node
     * 
     * Returns:
     *      void
    */
    void freeListR(MazeNode * curNode);

    /*
     * Description:
     *      Copies all nodes in the CLL recursively. Starts with second element and finishes with the "first"
     * 
     * Arguments:
     *      (IN) const CLL & -- CLL to copy
     *      (IN) MazeNode * -- current item to copy
     * 
     * Returns:
     *      void
    */
    void copycllR(const CLL & cllToCopy, MazeNode * curItem);

    /*
     * Description:
     *      Retreives a MazeNode from the CLL given the event name recursively. Starts with second element and finishes with the "first"
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to retreive
     *      (IN) MazeNode * -- current item to check
     * 
     * Returns:
     *      MazeNode * -- pointer to the MazeNode (nullptr if not found)
    */
    MazeNode * retreiveR(const std::string & eventName, MazeNode * curNode);

    /*
     * Description:
     *      Displays all nodes in list recursively. Starts with second element and finishes with the "first"
     * 
     * Arguments:
     *      (IN) MazeNode * -- pointer to the current node
     * 
     * Returns:
     *      None
    */
    void displayR(MazeNode * curNode) const;

    MazeNode * head; //Head of the CLL
    int size; //Total number of events
};
//********************************************************************************
//END CLL
//********************************************************************************