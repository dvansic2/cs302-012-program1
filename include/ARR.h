#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class declarations that make up the ARR
 */

#include "event.h"

#define ARR_DEFAULT_SIZE 1

//********************************************************************************
//BEGIN LLL
//********************************************************************************
/*
 * Description:
 *      LLL class that contains a list of HorseRideNodes
 * 
 * Arguments:
 *      None
*/
class LLL
{
public:
    LLL();
    ~LLL();
    LLL(const LLL & lllToCopy);

    /*
     * Description:
     *      Adds a new HorseRideNode to the end the LLL
     * 
     * Arguments:
     *      (IN) const char * -- name of the event
     *      (IN) const float -- price of the event
     *      (IN) const int -- start time of the event
     *      (IN) const int -- end time of the event
     *      (IN) const int -- minimum age of the event
     *      (IN) const char * -- name of the horse
     * 
     * Returns:
     *      true -- event added
     *      false -- event not added
     */
    bool insert(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName);

    /*
     * Description:
     *      Removes a HorseRideNode from the LLL given the event name
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to remove
     * 
     * Returns:
     *      true -- event removed
     *      false -- event not removed
    */
    bool remove(const std::string & eventName);

    /*
     * Description:
     *      Removes all HorseRideNodes from the LLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true -- all events removed
     *      false -- no events removed
    */
    bool removeAll(void);

    /*
     * Description:
     *      Retreives a HorseRideNode from the LLL given the event name
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to retreive
     * 
     * Returns:
     *      HorseRideNode * -- pointer to the HorseRideNode (nullptr if not found)
    */
    HorseRideNode * retreive(const std::string & eventName);

    /*
     * Description:
     *      Displays all HorseRideNodes in the LLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      void
    */
    void display(void) const;

    /*
     * Description:
     *      Returns the number of nodes in the LLL
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int -- number of nodes in the LLL
    */
    int getSize(void) const;

   
private:
    /*
     * Description:
     *      Frees all nodes in the LLL recursively
     * 
     * Arguments:
     *      (IN) HorseRideNode * -- pointer to the current node
     * 
     * Returns:
     *      void
    */
    void freeListR(HorseRideNode * curNode);

    /*
     * Description:
     *      Copies all nodes in the LLL recursively
     * 
     * Arguments:
     *      (IN) const LLL & -- LLL to copy
     * 
     * Returns:
     *      void
    */
    void copylllR(HorseRideNode * curItem);

    /*
     * Description:
     *      Retreives a HorseRideNode from the LLL given the event name recursively
     * 
     * Arguments:
     *      (IN) const std::string -- name of the event to retreive
     *      (IN) HorseRideNode * -- current item to check
     * 
     * Returns:
     *      HorseRideNode * -- pointer to the HorseRideNode (nullptr if not found)
    */
    HorseRideNode * retreiveR(const std::string & eventName, HorseRideNode * curNode);

    /*
     * Description:
     *      Displays all nodes in list recursively
     * 
     * Arguments:
     *      (IN) HorseRideNode * -- pointer to the current node
     * 
     * Returns:
     *      None
    */
    void displayR(HorseRideNode * curNode) const;

    HorseRideNode * head; //Head of the LLL
    HorseRideNode * tail; //Tail of the LLL
    int size; //Number of nodes in the LLL
};
//********************************************************************************
//END LLL
//********************************************************************************


//********************************************************************************
//BEGIN ARR
//********************************************************************************
/*
 * Description:
 *      ARR class that contains a dynamic array of LLLs
 * 
 * Arguments:
 *      int -- size of the dynamic array (default 1)
*/
class ARR
{
    public:
        ARR(const int size = ARR_DEFAULT_SIZE);
        ~ARR();
        ARR(const ARR & aarToCopy);

        /*
         * Description:
         *      Adds a new HorseRideNode to the end the LLL at the given index
         * 
         * Arguments:
         *      (IN) int -- index of the array to add to
         *      (IN) const char * -- name of the event
         *      (IN) const float -- price of the event
         *      (IN) const int -- start time of the event
         *      (IN) const int -- end time of the event
         *      (IN) const int -- minimum age of the event
         *      (IN) const char * -- name of the horse
         * 
         * Returns:
         *      true -- event added
         *      false -- event not added
         */
        bool insert(const int index, const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName);

        /*
         * Description:
         *      Removes a HorseRideNode from the LLL given the event name
         * 
         * Arguments:
         *      (IN) int -- index of the array to retreive from
         *      (IN) const std::string -- name of the event to remove
         * 
         * Returns:
         *      true -- event removed
         *      false -- event not removed
        */
        bool remove(const int index, const std::string & eventName);

        /*
         * Description:
         *      Removes all HorseRideNodes from the array
         * 
         * Arguments:
         *      None
         * 
         * Returns:
         *      true -- all events removed
         *      false -- no events removed
        */
        bool removeAll(void);

        /*
         * Description:
         *      Returns if the LLL at the given index is empty
         * 
         * Arguments:
         *      (IN) int -- index of the array to look at
         * 
         * Returns:
         *      true -- empty
         *      false -- not empty
        */
        int isEmpty(int index) const;

        /*
         * Description:
         *      Retreives a HorseRideNode from the LLL given the event name at the given index of the array
         * 
         * Arguments:
         *      (IN) int -- index of the array to retreive from
         *      (IN) const std::string -- name of the event to retreive
         * 
         * Returns:
         *      HorseRideNode * -- pointer to the HorseRideNode (nullptr if not found)
        */
        HorseRideNode * retreive(const int index, const std::string & eventName) const;

        /*
         * Description:
         *      Displays all HorseRideNodes in the array
         * 
         * Arguments:
         *      None
         * 
         * Returns:
         *      void
        */
        void display(void) const;

    private:
        /*
         * Description:
         *      Copies all LLLs in the array recursively
         * 
         * Arguments:
         *      (IN) const ARR & -- array to copy
         *      (IN) int -- current index
         * 
         * Returns:
         *      void
        */
        void copyArrR(const ARR & arrToCopy, int curIndex);

        /*
         * Description:
         *      Frees all indexes in the array recursively
         * 
         * Arguments:
         *      (IN) int -- current index
         * 
         * Returns:
         *      void
        */
        void freeArrR(int curIndex);

        /*
         * Description:
         *      Prints all indexes in the array recursively
         * 
         * Arguments:
         *      (IN) int -- current index
         * 
         * Returns:
         *      None
        */
        void printArrR(int curIndex) const;

        int size; //Size of the array
        LLL ** lllArray;  //Array of LLL pointers
};