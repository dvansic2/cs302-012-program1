#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 4/6/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class declarations that make up the participantManager
 */

#include <list>
#include <string>

//********************************************************************************
//BEGIN Participant 
//  NOTE: This class exists purely b/c I cant use a struct, would have otherwise
//            used a struct
//********************************************************************************
class Participant
{
public:
    Participant(const std::string & name, const int age);
    ~Participant(void);

    /*
     * Description:
     *      Prints all the stats of a participant to the console
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      none
     */
    void display();

    /*
     * Description:
     *      Returns the age of a participant
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int -- age of participant
     */
    int getAge(void);

    /*
     * Description:
     *      Returns the name of a participant
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      string -- name of participant
     */
    std::string getName(void);

private:
    std::string name; //Full name of participant
    int age;          //Age of participant
};
//********************************************************************************
//END Participant
//********************************************************************************


//********************************************************************************
//BEGIN ParticipantManager
//********************************************************************************
class ParticipantManager
{
public:
    ParticipantManager(void);
    ~ParticipantManager(void);
    ParticipantManager(const ParticipantManager & pmToCopy);

    /*
     * Description:
     *      Adds a participant to the list
     *
     * Arguments:
     *     (IN) String  -- Full name of participant (must not be empty)
     *     (IN) int     -- Integer age of participant
     * 
     * Returns:
     *      true    -- participant added
     *      false   -- participant not added
     */
    bool add(const std::string & name, const int age);

    /*
     * Description:
     *      Removes a participant from the list
     *  
     * Arguments:
     *      (IN) string -- Full name of participant to remove (must not be empty)
     * 
     * Returns:
     *      true    -- paricipant removed
     *      false   -- participant not removed
     */
    bool remove(const std::string & name);

    /*
     * Description:
     *      Returns if a participant exists
     * 
     * Arguments:
     *      (IN) string -- Full name of participant (must not be empty)
     * 
     * Returns:
     *      true -- participant exists
     *      false -- participant does not exist
     */
    bool participantExists(const std::string & name) const;

    /*
     * Description:
     *      Prints all participants to the console
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void printAllParticipants(void) const;

    /*
     * Description:
     *      Returns the total number of participants
     * 
     * Arguments:
     *     None
     * 
     * Returns:
     *      int -- total number of participants
     */
    int getTotal(void) const;

private:
    std::list<Participant*> participantsList; //List of participants

    /*
     * Description:
     *      Frees all memory in the list recursively
     * 
     * Arguments:
     *      (IN) int -- number of items left to free
     * 
     * Returns:
     *      None
     */
    void freeListR(int itemsLeft);

    /*
     * Description:
     *      Recursively copies a list of participants
     *      
     *      personal note: called from the object that is being copied to
     * 
     * Arguments:
     *      ParticipantManager & -- participant manager object to copy from
     *      list<Participant*>::const_iterator -- iterator from list to copy from
     * 
     * Returns:
     *      None
    */
    void copyListR(const ParticipantManager & pmToCopy, std::list<Participant*>::const_iterator it);

    /*
     * Description:
     *      Returns a participant object that matches a given name
     * 
     * Arguments:
     *      (IN) string -- Full name of participant (must not be empty)
     * 
     * Returns:
     *      Participant * -- participant object
     *      nullptr -- participant not found
     */
    Participant * returnParticipant(const std::string & name) const;

    /*
     * Description:
     *      seraches for and returns a participant object recursively
     * 
     * Arguments:
     *      (IN) list<Participant *>::const_iterator -- iterator to list
     *      (IN) string -- Full name of participant to find
     * 
     * Returns:
     *      Participant * -- participant object
     *      nullptr -- participant not found
     */
    Participant * returnParticipantR(std::list<Participant *>::const_iterator it, const std::string & nameToFind) const;
};
