#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 4/6/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class declarations that make up the event hierarchy
 */
#include <vector>
#include "participant.h"

//Event defaults
#define DFLT_EVNT_PARTICIPANT_LMT   15
#define DFLT_EVNT_MIN_AGE           0
#define DFLT_EVNT_PRICE             0.0
#define DFLT_EVNT_STRT_TIME         8
#define DFLT_EVNT_END_TIME_OFFSET   1
#define DFLT_EVNT_NAME              "Unnamed Event"

//PettingZoo defaults
#define DFLT_PETTING_ZOO_GOATS      3
#define DFLT_PETTING_ZOO_SHEEP      2
#define DFLT_PETTING_ZOO_DUCKS      4

//HorseRide defaults
#define DFLT_HORSE_NAME             "Unnamed Horse"

enum mazeTypes
{
    CORN,
    HEDGE,

    TYPE_COUNT
};

enum mazeDifficulties
{
    EASY,
    MEDIUM,
    HARD,

    DIFF_COUNT
};

//********************************************************************************
//BEGIN Event
//********************************************************************************
/*
 * Description:
 *      Event class that holds the information for an event
 * 
 * Arguments:
 *      char * eventName -- the name of the event (must not be empty or null)
 *      float price      -- the price of the event (must be greater than 0)
 *      int startTime    -- the start time of the event in 24hr time (must be greater than or equal to 0)
 *      int endTime      -- the end time of the event in 24hr time (must be greater than the start time)
 *      int minAge       -- the minimum age to participate in the event (must be greater than or equal to 0)    
*/
class Event
{
public:
    Event(const char * eventName, const float price, const int startTime, const int endTime, const int minAge);
    ~Event();
    Event(const Event & eventToCopy);

    /*
     * Description:
     *      Retruns whether or not the event has reached its limit of participants
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true -- limit reached
     *      false -- limit not reached
     */
    bool limitReached(void) const;

    /*
     * Description:
     *      Changes the limit of participants for the event
     * 
     * Arguments:
     *      (IN) int    -- the new limit for the event (must be greater than 0)
     * 
     * Returns:
     *     true -- limit changed
     *     false -- limit not changed
     */
    bool changeLimit(const int newLimit);

    /*
     * Description:
     *      Returns whether or not a person is in the event from a given name
     * 
     * Arguments:
     *      (IN) const std::string& -- the name of the person to check for (must not be empty)
     * 
     * Returns:
     *      true -- person in event
     *      false -- person not in event
    */
    bool personInEvent(const std::string & name) const;

    /*
     * Description:
     *      Adds a participant to the event
     *      Checks if limit has been reached, if the person is already in the event, and if the person is old enough 
     * 
     * Arguments:
     *      (IN) const std::string & -- the name of the participant (must not be empty)
     *      (IN) const int -- the age of the participant (must be greater than 0)
     * 
     * Returns:
     *      true -- participant added
     *      false -- participant not added
     *      
    */
    bool addParticipantWithChecks(const std::string & name, const int age);

    /*
     * Description:
     *      Removes a participant from the event based on a given name
     * 
     * Arguments:
     *      (IN) const std::string & -- the name of the participant to remove (must not be empty)
     * 
     * Returns:
     *      true -- participant removed
     *      false -- participant not removed
    */
    bool removeParticipant(const std::string & name);

    /*
     * Description:
     *      Returns the total number of participants in the event
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int -- total number of participants
     */
    int getTotalParticipants(void) const;

    /*
     * Description:
     *      Prints out the stats of the event
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void printStats(void) const;

    /*
     * Description:
     *      Returns the event name
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      char * -- pointer to event name c-string 
    */
    const char * getEventName(void) const;

private:
    ParticipantManager participants;

protected:
    char *              eventName;      //Name of the event
    float               price;          //Price of the event
    std::vector<int>    startEndTime;   //Start and end time of the event
    int                 limit;          //Limit of participants for the event
    int                 minAge;         //Minimum age to participate in the event
};
//********************************************************************************
//END Event
//********************************************************************************


//********************************************************************************
//BEGIN PettingZoo
//********************************************************************************
/*
 * Description:
 *      PettingZoo class that holds the information for a petting zoo event
 * 
 * Arguments:
 *      all arguments from the Event class, See Event class for more information  
*/
class PettingZoo : public Event
{
public:
    PettingZoo(const char * eventName, const float price, const int startTime, const int endTime, const int minAge);
    ~PettingZoo(void);

    /*
     * Description:
     *      Changes the number of animals in the petting zoo. A value of -1 or lower will not change the number of that animal
     * 
     * Arguments:
     *      (IN) const int numOfGoats -- the new number of goats in the petting zoo
     *      (IN) const int numOfSheep -- the new number of sheep in the petting zoo
     *      (IN) const int numOfDucks -- the new number of ducks in the petting zoo 
     * 
     * Returns:
     *      true -- animals changed
     *      false -- animals not changed
    */
    bool changeAnimals(const int numOfGoats, const int numOfSheep, const int numOfDucks);

    /*
     * Description:
     *      Prints out the amount of each animal in the petting zoo
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void displayAnimals(void) const;

    /*
     * Description:
     *      Prints out all the stats of the petting zoo (including base event stats)
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void printStats(void) const;

private:
    int numOfGoats; //Number of goats in the petting zoo
    int numOfSheep; //Number of sheep in the petting zoo
    int numOfDucks; //Number of ducks in the petting zoo
};
//********************************************************************************
//END PettingZoo
//********************************************************************************


//********************************************************************************
//BEGIN Maze
//********************************************************************************
/*
 * Description:
 *      Maze class that holds the information for a Maze event
 * 
 * Arguments:
 *      mazeType --  enum of the type of maze (corn or hedge)    
 *      mazeDifficulties -- enum of the difficulty of the maze (easy, medium, or hard)
 * 
 *      all arguments from the Event class, See Event class for more information  
*/
class Maze : public Event
{
public:
    Maze(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty);
    ~Maze(void);

    /*
     * Description:
     *      Change the difficulty of the maze (imagine it as changing the maze itself)
     * 
     * Arguments:
     *      (IN) const mazeDifficulties -- enum for the new difficulty of the maze
     * 
     * Returns:
     *      true -- difficulty changed
     *      false -- difficulty not changed  
    */
    bool changeDifficulty(const mazeDifficulties newDifficulty);

    /*
     * Description:
     *      Checks for lost participants in the maze and returns the number of lost participants
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int -- number of lost participants
    */
    int checkForLostParticipants(void);

    /*
     * Description:
     *      Helps lost participants find their way out of the maze
     *      note: will reset the number of lost participants to 0
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true -- participants helped
     *      false -- no participants to help
    */
    bool helpLostParticipants(void);

    /*
     * Description:
     *      Prints out the stats of the maze
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void printStats(void) const;

private:
    mazeDifficulties  difficulty;                         //Difficulty of the maze
    mazeTypes        type;                               //Type of the maze
    int             curNumOfLostParticipants;           //Current number of lost participants in the maze
    int             totalNumOfLostParticipantsHelped;   //Number of lost participants helped over time
};
//********************************************************************************
//END Maze
//********************************************************************************


//********************************************************************************
//BEGIN HorseRide
//********************************************************************************
/*
 * Description:
 *      HorseRide class that holds the information for a HorseRide event
 * 
 * Arguments:
 *      char * horseName -- the name of the horse (must not be empty or null)
 *      
 *      all arguments from the Event class, See Event class for more information  
*/
class HorseRide : public Event
{
public:
    HorseRide(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName);
    ~HorseRide(void);
    HorseRide(const HorseRide & otherHr);

    /*
     * Description:
     *      Checks if a given age can ride the horse
     * 
     * Arguments:
     *      (IN) const int -- the age of the person (must be greater than 0)
     * 
     * Returns:
     *      true -- person can ride
     *      false -- person cannot ride
     *      
    */
    bool canRide(const int age) const;

    /*
     * Description:
     *      Changes the name of the horse
     * 
     * Arguments:
     *      (IN) const char * -- the new name of the horse
     * 
     * Returns:
     *      true -- name changed
     *      false -- name not changed
    */
    bool changeHorse(const std::string & newHorseName);

    /*
     * Description:
     *      Prints out the stats of the horse ride
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void printStats(void) const;

private:
    char * horseName;   //Name of the horse
};
//********************************************************************************
//END HorseRide
//********************************************************************************


//********************************************************************************
//BEGIN HorseRideNode
//********************************************************************************
/*
 * Description:
 *      Node class that holds the information for a HorseRide event for a LLL
 * 
 * Arguments:
 *      HorseRideNode * next -- the next node in the list
 *      HorseRideNode * prev -- the previous node in the list
*/
class HorseRideNode : public HorseRide
{
public:
    HorseRideNode(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, const char * horseName);
    ~HorseRideNode(void);

    /*
     * Description:
     *      Returns the next node in the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      HorseRideNode * -- the next node in the list
    */
    HorseRideNode * getNext(void) const;

    /*
     * Description:
     *      Changes the next node in the list
     * 
     * Arguments:
     *      (IN) HorseRideNode * -- the new next node in the list
     * 
     * Returns:
     *      None
    */
    void setNext(HorseRideNode * newNext);

    /*
     * Description:
     *      Changes the previous node in the list
     * 
     * Arguments:
     *      (IN) HorseRideNode * -- the new previous node in the list
     * 
     * Returns:
     *      None
    */
    void setPrev(HorseRideNode * newPrev);

    /*
     * Description:
     *      Returns the previous node in the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      HorseRideNode * -- the previous node in the list
    */
    HorseRideNode * getPrev(void) const;

private:
    HorseRideNode * next; //Next node in the list
    HorseRideNode * prev; //Previous node in the list
};
//********************************************************************************
//END HorseRideNode
//********************************************************************************


//********************************************************************************
//BEGIN MazeNode
//********************************************************************************
/*
 * Description:
 *      Node class that holds the information for a Maze event for a CLL
 * 
 * Arguments:
 *      MazeNode * next -- the next node in the list
 *      MazeNode * prev -- the previous node in the list
*/
class MazeNode : public Maze
{
public:
    MazeNode(const char * eventName, const float price, const int startTime, const int endTime, const int minAge, mazeTypes type, mazeDifficulties difficulty);
    ~MazeNode(void);

    /*
     * Description:
     *      Returns the next node in the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      MazeNode * -- the next node in the list
    */
    MazeNode * getNext(void) const;

    /*
     * Description:
     *      Changes the next node in the list
     * 
     * Arguments:
     *      (IN) MazeNode * -- the new next node in the list
     * 
     * Returns:
     *      None
    */
    void setNext(MazeNode * newNext);

    /*
     * Description:
     *      Changes the previous node in the list
     * 
     * Arguments:
     *      (IN) MazeNode * -- the new previous node in the list
     * 
     * Returns:
     *      None
    */
    void setPrev(MazeNode * newPrev);

    /*
     * Description:
     *      Returns the previous node in the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      MazeNode * -- the previous node in the list
    */
    MazeNode * getPrev(void) const;

private:
    MazeNode * next; //Next node in the list
    MazeNode * prev; //Previous node in the list
};
//********************************************************************************
//END MazeNode
//********************************************************************************