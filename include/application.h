#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 4/14/24
 * Class: CS302
 * Assignment: Program 1
 * Description:
 *      Contains the class declarations that make up the application class
 */
#include <vector>
#include "ARR.h"
#include "CLL.h"

enum eventTypes
{
    PETTING_ZOO = 1,
    MAZE,
    HORSE_RIDE,

    EVENT_COUNT
};

class App
{
public:
    App();
    ~App();

    /*
     * Description:
     *      Main menu of the application
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int
     *          >= 0 -- application exited cleanly
     *          < 0 -- application exited with an error    
    */
    int displayMainMenu();
    

private:
    /*
     * Description:
     *      Adds a new event to the appropriate data structure
     * 
     * Arguments:
     *          (IN) const evnetTypes -- type of the event (PETTING_ZOO, MAZE, HORSE_RIDE)   
     * 
     * Returns:
     *      true -- event added
     *      false -- event not added
    */
    bool addEvent(const eventTypes type);

    /*
     * Description:
     *      Removes an event from the appropriate data structure
     * 
     * Arguments:
     *      (IN) const evnetTypes -- type of the event (PETTING_ZOO, MAZE, HORSE_RIDE)
     * 
     * Returns:
     *      true -- event removed
     *      false -- event not removed
    */
    bool closeEvent(const eventTypes type);

    /*
     * Description:
     *      Adds a participant to the appropriate event
     * 
     * Arguments:
     *      (IN) const eventTypes -- type of the event (PETTING_ZOO, MAZE, HORSE_RIDE)
     * 
     * Returns:
     *      true -- participant added
     *      false -- participant not added
    */
    bool addParticipant(const eventTypes eventType);
    
    /*
     * Description:
     *      Removes a participant from the appropriate event
     * 
     * Arguments:
     *      (IN) const eventTypes -- type of the event (PETTING_ZOO, MAZE, HORSE_RIDE)
     * 
     * Returns:
     *      true -- participant removed
     *      false -- participant not removed
    */
    bool removeParticipant(const eventTypes eventType);

    /*
     * Description:
     *      Lists all events of a given type
     * 
     * Arguments:
     *      (IN) const eventTypes -- type of the event (PETTING_ZOO, MAZE, HORSE_RIDE)
     * 
     * Returns:
     *      None
    */
    void listEvents(const eventTypes eventType) const;

    /*
     * Description:
     *      Validates a number option
     * 
     * Arguments:
     *      (IN) const std::string -- user input
     *      (IN) const int -- max number of digits in the number (default is 1)
     * 
     * Returns:
     *      int -- validated number (returns INT_MIN if not valid)
    */
    int validateNumOption(const std::string & userInput, int maxDigits = 1) const;

    /*
     * Description:
     *      Menu for event specific options
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true -- event specific menu exited cleanly
     *      false -- event specific menu exited with an error
    */
    bool eventSpecificMenu(void);

    /*
     * Description:
     *      Clears the screen
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void clearScreen() const;

    /*
     * Description:
     *      Waits for the user to press enter
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void waitForEnter() const;

    /*
     * Description:
     *      Frees all memory allocated for the petting zoo events
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void freeListR(int itemsLeft);

    /*
     * Description:
     *     Returns a pettingZoo event object that matches a given name
     * 
     * Arguments:
     *      (IN) string -- Full name of event (must not be empty)
     * 
     * Returns:
     *      PettingZoo * -- petting zoo object
     *      nullptr -- petting zoo not found
     *      
    */
    PettingZoo * returnPettingZoo(const std::string & eventName) const;

    std::list<PettingZoo*> pettingZooEvents; //Vector of petting zoo events
    CLL mazeEvents; //Circular linked list of maze events
    ARR horseRideEvents; //Array of LLL containing horse ride events

    enum mainMenuOptions
    {
        ADD_EVENT = 1,
        CLOSE_EVENT,
        ADD_PARTICIPANT,
        REMOVE_PARTICIPANT,
        LIST_EVENTS,
        EVENT_SPECIFIC,
        EXIT,

        OPTION_COUNT
    };

    enum eventSpecificOptions
    {
        PETTING_ZOO_ANIMAL_CNG = 1,
        MAZE_FIND_LOST,
        MAZE_SAVE_LOST,
        HORSE_RIDE_NAME_HORSE,

        EVENT_SPECIFIC_COUNT
    };

};